%%------------KONSTANTY A FUNKCE------------------------------------------------
% definice promennych
RWY06 = [-756405.081531191,-1039509.71370995,366.4];
RWY24 = [-752853.309335832,-1038422.16470496,352.8];
RWY12 = [-754958.919015969,-1039019.44287027,353.6];
RWY30 = [-752656.998205195,-1041310.92429098,375.5];
h_letiste = 360;


% definice funkci
function bod = eastNorth(RWY)
  bod = [-RWY(2), -RWY(1), RWY(3)];
endfunction;

RWY06 = eastNorth(RWY06);
RWY12 = eastNorth(RWY12);
RWY24 = eastNorth(RWY24);
RWY30 = eastNorth(RWY30);

function s = smernik(bod1, bod2)
  as = atan2(bod2(2) - bod1(2), bod2(1) - bod1(1));
  if (as < 0)
    as += 2*pi;
  end;
 
  asl = as - pi/2;
  asr = as + pi/2;
  asa = as + pi;

  if (asr > 2*pi)
    asr -= 2*pi;
  end;
  
  if (asa > 2*pi)
    asa -= 2*pi;
  end;
  s = [as, asl, asr, asa];
endfunction;

function d = vzdalenost(A, B)
   d = sqrt((A(1)-B(1)) * (A(1)-B(1)) + ((A(2)-B(2)) * (A(2)-B(2))));
endfunction;   

function point = rajon(d, alfa, RWY)
  point = [RWY(1) + d * cos(alfa),RWY(2) + d * sin(alfa),RWY(3)];
endfunction;

function point = sikmyRajon(d,alfa,RWY,h)
  point = rajon(d,alfa, RWY);
  point(3) += h;
endfunction;

function ulozPolygon(fileName, polygons)
  file = fopen(fileName, 'w');
  fdisp(file,'id;geometry');
  n = size(polygons)(2)/3;
  for i = 1:n
    fprintf(file,'%i;POLYGON((', i);    
    k = (i-1)*3;
    for j = 1:size(polygons)(1) 
      if (j != size(polygons)(1)) 
        fprintf(file,'%f %f %f,',polygons(j,k+1), polygons(j,k+2),polygons(j,k+3));  
      else
        fprintf(file,'%f %f %f))\n',polygons(j,k+1), polygons(j,k+2),polygons(j,k+3));
      end; 
    end;
  end;
  fclose(file);
endfunction;

function ulozJakoVrstevnice(fileName, lines)
  file=fopen(fileName,'w');
  fdisp(file,'id;geometry;elevation');
  n=size(lines)(2)/3;
  for i=1:n
    fprintf(file, '%i;LINESTRING(', i);
    k = (i-1)*3;
    for j=1:size(lines)(1)
      if (j != size(lines)(1))
        fprintf(file,'%f %f,', lines(j,k+1), lines(j,k+2));  
      else
        fprintf(file,'%f %f);%f\n', lines(j,k+1), lines(j,k+2), lines(j,k+3));  
      end;  
    end;  
  end;  
  fclose(file);
endfunction;

function ulozJakoBody(fileName, polygons)
  file=fopen(fileName,'w');
  fdisp(file, 'id;geometry;elevation');
  n=size(polygons)(1);
  for i = 1:n
    fprintf(file,'%i;POINT(%f %f);%f',i,polygons(i,1), polygons(i,2), polygons(i,3));
    if i != n
      fprintf(file,'\n');
    end;
  end;
  fclose(file);  
endfunction;

function ulozPolygonSDirou(fileName, polygons_out, polygons_in)%uklada n polygonu, kazdy s jednou dirou
  file=fopen(fileName,'w');
  fdisp(file,'id;geometry');
  n = size(polygons_out)(2)/3;
  for i = 1:n
    fprintf(file,'%i;POLYGON((', i);    
    k = (i-1)*3;
    for j = 1:size(polygons_out)(1) 
      if (j != size(polygons_out)(1)) 
        fprintf(file,'%f %f %f,',polygons_out(j,k+1), polygons_out(j,k+2),polygons_out(j,k+3));  
      else
        fprintf(file,'%f %f %f),(',polygons_out(j,k+1), polygons_out(j,k+2),polygons_out(j,k+3));
      end; 
    end;
    for j = 1:size(polygons_in)(1) 
      if (j != size(polygons_in)(1)) 
        fprintf(file,'%f %f %f,',polygons_in(j,k+1), polygons_in(j,k+2),polygons_in(j,k+3));  
      else
        fprintf(file,'%f %f %f))\n',polygons_in(j,k+1), polygons_in(j,k+2),polygons_in(j,k+3));
      end; 
    end;
  end;
  fclose(file);
endfunction;

%%------------DRAHY-------------------------------------------------------------
%promenne pro vypocet drah
d_axis = 400;
d_norm = 300;

% okoli bodu 06
s06_24 = smernik(RWY06, RWY24);
sig_06_l = s06_24(2);
sig_06_r = s06_24(3);
sig_06_a = s06_24(4);

RWY06l = rajon(d_norm, sig_06_l, RWY06);
RWY06r = rajon(d_norm, sig_06_r, RWY06);
RWY06a = rajon(d_axis, sig_06_a, RWY06);
RWY06al = rajon(d_norm, sig_06_l, RWY06a);
RWY06ar = rajon(d_norm, sig_06_r, RWY06a);

% okoli bodu 24
s24_06 = smernik(RWY24, RWY06);
sig_24_l = s24_06(2);
sig_24_r = s24_06(3);
sig_24_a = s24_06(4);

RWY24l = rajon(d_norm, sig_24_l, RWY24);
RWY24r = rajon(d_norm, sig_24_r, RWY24);
RWY24a = rajon(d_axis, sig_24_a, RWY24);
RWY24al = rajon(d_norm, sig_24_l, RWY24a);
RWY24ar = rajon(d_norm, sig_24_r, RWY24a);

% okoli bodu 12
s12_30 = smernik(RWY12, RWY30);
sig_12_l = s12_30(2);
sig_12_r = s12_30(3);
sig_12_a = s12_30(4);

RWY12l = rajon(d_norm, sig_12_l, RWY12);
RWY12r = rajon(d_norm, sig_12_r, RWY12);
RWY12a = rajon(d_axis, sig_12_a, RWY12);
RWY12al = rajon(d_norm, sig_12_l, RWY12a);
RWY12ar = rajon(d_norm, sig_12_r, RWY12a);

% okoli bodu 30
s30_12 = smernik(RWY30, RWY12);
sig_30_l = s30_12(2);
sig_30_r = s30_12(3);
sig_30_a = s30_12(4);

RWY30l = rajon(d_norm, sig_30_l, RWY30);
RWY30r = rajon(d_norm, sig_30_r, RWY30);
RWY30a = rajon(d_axis, sig_30_a, RWY30);
RWY30al = rajon(d_norm, sig_30_l, RWY30a);
RWY30ar = rajon(d_norm, sig_30_r, RWY30a);

polygon1230 = [eastNorth(RWY12al); eastNorth(RWY12l); eastNorth(RWY30r); eastNorth(RWY30ar); eastNorth(RWY30a); eastNorth(RWY30al); eastNorth(RWY30l); eastNorth(RWY12r); eastNorth(RWY12ar); eastNorth(RWY12a); eastNorth(RWY12al)];
polygon0624 = [eastNorth(RWY06al); eastNorth(RWY06l); eastNorth(RWY24r); eastNorth(RWY24ar); eastNorth(RWY24a); eastNorth(RWY24al); eastNorth(RWY24l); eastNorth(RWY06r); eastNorth(RWY06ar); eastNorth(RWY06a); eastNorth(RWY06al)];

line06a = [eastNorth(RWY06al);eastNorth(RWY06a);eastNorth(RWY06ar)];
line06 = [eastNorth(RWY06l);eastNorth(RWY06);eastNorth(RWY06r)];
line24 = [eastNorth(RWY24l);eastNorth(RWY24);eastNorth(RWY24r)];
line24a = [eastNorth(RWY24al);eastNorth(RWY24a);eastNorth(RWY24ar)];

line12a = [eastNorth(RWY12al);eastNorth(RWY12a);eastNorth(RWY12ar)];
line12 = [eastNorth(RWY12l);eastNorth(RWY12);eastNorth(RWY12r)];
line30 = [eastNorth(RWY30l);eastNorth(RWY30);eastNorth(RWY30r)];
line30a = [eastNorth(RWY30al);eastNorth(RWY30a);eastNorth(RWY30ar)];


% ulozeni do souboru (polygony oddeleny carkou)
filename = '/home/michal/Projects/LetisteProjekt/modelDrah/polygonyDrah.csv';
ulozPolygon(filename, [polygon0624, polygon1230])

filename = '/home/michal/Projects/LetisteProjekt/proVypocet/polygon0624.csv';
ulozPolygon(filename, polygon0624)
filename = '/home/michal/Projects/LetisteProjekt/proVypocet/polygon1230.csv';
ulozPolygon(filename, polygon1230)

filename = '/home/michal/Projects/LetisteProjekt/proVypocet/vrstevnice0624.csv';
ulozJakoVrstevnice(filename, [line06a, line06, line24, line24a]);
filename = '/home/michal/Projects/LetisteProjekt/proVypocet/vrstevnice1230.csv';
ulozJakoVrstevnice(filename, [line12a, line12, line30, line30a]);


filename = '/home/michal/Projects/LetisteProjekt/proVypocet/polygon0624body.csv';
ulozJakoBody(filename, polygon0624)
filename = '/home/michal/Projects/LetisteProjekt/proVypocet/polygon1230body.csv';
ulozJakoBody(filename, polygon1230)



filename = '/home/michal/Projects/LetisteProjekt/modelDrah/polygonyDrahBody.csv';
ulozJakoBody(filename, [polygon0624; polygon1230])

%%------------PRIBLIZOVACI PROSTOR----------------------------------------------
%promenne pro lichobezniky priblizovaciho prostoru
d_top = 150*62.5;
d_end = 15000;
uhel = 0.14889;
d_side_top = d_top/cos(uhel);
d_side_end = d_end/cos(uhel);
h_top = 150;


% vypocet lichobezniku za RWY 06
sig_06a_e = sig_06_a;
sig_06al_el = sig_06a_e + uhel;
sig_06ar_er = sig_06a_e - uhel;

RWY06t = sikmyRajon(d_top, sig_06a_e, RWY06a, h_top);
RWY06e = sikmyRajon(d_end, sig_06a_e, RWY06a, h_top);
RWY06tl = sikmyRajon(d_side_top, sig_06al_el, RWY06al, h_top);
RWY06el = sikmyRajon(d_side_end, sig_06al_el, RWY06al, h_top);
RWY06tr = sikmyRajon(d_side_top, sig_06ar_er, RWY06ar, h_top);
RWY06er = sikmyRajon(d_side_end, sig_06ar_er, RWY06ar, h_top);

polygon06a06e = [eastNorth(RWY06al); eastNorth(RWY06a);eastNorth(RWY06ar);eastNorth(RWY06tr);eastNorth(RWY06er);eastNorth(RWY06e);eastNorth(RWY06el);eastNorth(RWY06tl);eastNorth(RWY06al)];
line06a = [eastNorth(RWY06al);eastNorth(RWY06ar)];
line06t = [eastNorth(RWY06tl);eastNorth(RWY06tr)];
line06e = [eastNorth(RWY06el);eastNorth(RWY06er)];

% vypocet lichobezniku za RWY 24
sig_24a_e = sig_24_a;
sig_24al_el = sig_24a_e + uhel;
sig_24ar_er = sig_24a_e - uhel;

RWY24t = sikmyRajon(d_top, sig_24a_e, RWY24a, h_top);
RWY24e = sikmyRajon(d_end, sig_24a_e, RWY24a, h_top);
RWY24tl = sikmyRajon(d_side_top, sig_24al_el, RWY24al, h_top);
RWY24el = sikmyRajon(d_side_end, sig_24al_el, RWY24al, h_top);
RWY24tr = sikmyRajon(d_side_top, sig_24ar_er, RWY24ar, h_top);
RWY24er = sikmyRajon(d_side_end, sig_24ar_er, RWY24ar, h_top);

polygon24a24e = [eastNorth(RWY24al); eastNorth(RWY24a);eastNorth(RWY24ar);eastNorth(RWY24tr);eastNorth(RWY24er);eastNorth(RWY24e);eastNorth(RWY24el);eastNorth(RWY24tl);eastNorth(RWY24al)];
line24a = [eastNorth(RWY24al);eastNorth(RWY24ar)];
line24t = [eastNorth(RWY24tl);eastNorth(RWY24tr)];
line24e = [eastNorth(RWY24el);eastNorth(RWY24er)];

% vypocet lichobezniku za RWY 12
sig_12a_e = sig_12_a;
sig_12al_el = sig_12a_e + uhel;
sig_12ar_er = sig_12a_e - uhel;

RWY12t = sikmyRajon(d_top, sig_12a_e, RWY12a, h_top);
RWY12e = sikmyRajon(d_end, sig_12a_e, RWY12a, h_top);
RWY12tl = sikmyRajon(d_side_top, sig_12al_el, RWY12al, h_top);
RWY12el = sikmyRajon(d_side_end, sig_12al_el, RWY12al, h_top);
RWY12tr = sikmyRajon(d_side_top, sig_12ar_er, RWY12ar, h_top);
RWY12er = sikmyRajon(d_side_end, sig_12ar_er, RWY12ar, h_top);

polygon12a12e = [eastNorth(RWY12al); eastNorth(RWY12a);eastNorth(RWY12ar);eastNorth(RWY12tr);eastNorth(RWY12er);eastNorth(RWY12e);eastNorth(RWY12el);eastNorth(RWY12tl);eastNorth(RWY12al)];
line12a = [eastNorth(RWY12al);eastNorth(RWY12ar)];
line12t = [eastNorth(RWY12tl);eastNorth(RWY12tr)];
line12e = [eastNorth(RWY12el);eastNorth(RWY12er)];

% vypocet lichobezniku za RWY 030
sig_30a_e = sig_30_a;
sig_30al_el = sig_30a_e + uhel;
sig_30ar_er = sig_30a_e - uhel;

RWY30t = sikmyRajon(d_top, sig_30a_e, RWY30a, h_top);
RWY30e = sikmyRajon(d_end, sig_30a_e, RWY30a, h_top);
RWY30tl = sikmyRajon(d_side_top, sig_30al_el, RWY30al, h_top);
RWY30el = sikmyRajon(d_side_end, sig_30al_el, RWY30al, h_top);
RWY30tr = sikmyRajon(d_side_top, sig_30ar_er, RWY30ar, h_top);
RWY30er = sikmyRajon(d_side_end, sig_30ar_er, RWY30ar, h_top);

polygon30a30e = [eastNorth(RWY30al); eastNorth(RWY30a);eastNorth(RWY30ar);eastNorth(RWY30tr);eastNorth(RWY30er);eastNorth(RWY30e);eastNorth(RWY30el);eastNorth(RWY30tl);eastNorth(RWY30al)];
line30a = [eastNorth(RWY30al);eastNorth(RWY30ar)];
line30t = [eastNorth(RWY30tl);eastNorth(RWY30tr)];
line30e = [eastNorth(RWY30el);eastNorth(RWY30er)];

% ulozeni polygonu op do souboru
filename = '/home/michal/Projects/LetisteProjekt/modelDrah/polygonyOP.csv';
ulozPolygon(filename, [polygon06a06e, polygon24a24e, polygon12a12e, polygon30a30e])
filename = '/home/michal/Projects/LetisteProjekt/proVypocet/polygon_lichobeznik_body.csv';
ulozJakoBody(filename, [polygon06a06e; polygon24a24e; polygon12a12e; polygon30a30e])

filename = '/home/michal/Projects/LetisteProjekt/proVypocet/polygon_lichobeznik_06.csv';
ulozPolygon(filename, polygon06a06e)
filename = '/home/michal/Projects/LetisteProjekt/proVypocet/body_lichobeznik_06.csv';
ulozJakoBody(filename, polygon06a06e)
filename = '/home/michal/Projects/LetisteProjekt/proVypocet/vrstevnice_lb_06.csv';
ulozJakoVrstevnice(filename, [line06a, line06t, line06e]);

filename = '/home/michal/Projects/LetisteProjekt/proVypocet/polygon_lichobeznik_12.csv';
ulozPolygon(filename, polygon12a12e)
filename = '/home/michal/Projects/LetisteProjekt/proVypocet/body_lichobeznik_12.csv';
ulozJakoBody(filename, polygon12a12e)
filename = '/home/michal/Projects/LetisteProjekt/proVypocet/vrstevnice_lb_12.csv';
ulozJakoVrstevnice(filename, [line12a, line12t, line12e]);

filename = '/home/michal/Projects/LetisteProjekt/proVypocet/polygon_lichobeznik_24.csv';
ulozPolygon(filename, polygon24a24e)
filename = '/home/michal/Projects/LetisteProjekt/proVypocet/body_lichobeznik_24.csv';
ulozJakoBody(filename, polygon24a24e)
filename = '/home/michal/Projects/LetisteProjekt/proVypocet/vrstevnice_lb_24.csv';
ulozJakoVrstevnice(filename, [line24a, line24t, line24e]);

filename = '/home/michal/Projects/LetisteProjekt/proVypocet/polygon_lichobeznik_30.csv';
ulozPolygon(filename, polygon30a30e)
filename = '/home/michal/Projects/LetisteProjekt/proVypocet/body_lichobeznik_30.csv';
ulozJakoBody(filename, polygon30a30e)
filename = '/home/michal/Projects/LetisteProjekt/proVypocet/vrstevnice_lb_30.csv';
ulozJakoVrstevnice(filename, [line30a, line30t, line30e]);


%%------------VNITRNI VODOROVNA PLOCHA------------------------------------------
r_kruh = 4000;
h_kruh = 45;
h_kruhova_op = h_letiste + h_kruh;

function kruh = aproximaceKruhu(stred, r, sig_l, sig_r, n) 
  %n je pocet vyseci v polygonu
  vysec = (sig_l - sig_r)/n;
  kruh(1,:) = eastNorth(rajon(r, sig_r, stred));
  sig = sig_r;
  for i = 1:n+1
    sig += vysec; 
    kruh(i+1,:) = eastNorth(rajon(r, sig, stred));
  end;  
endfunction;

%okoli 06
sig_06a_ka = sig_06_a;
sig_06a_kl = sig_06a_ka + pi/2;
sig_06a_kr = sig_06a_ka - pi/2;
stred06 = RWY06a;
stred06(3) = h_kruhova_op;
kruh06 = aproximaceKruhu(stred06, r_kruh, sig_06a_kl, sig_06a_kr,90);
uzavreniKruhu06 = eastNorth(rajon(r_kruh, sig_06a_kr, stred06));

%okoli 24
sig_24a_ka = sig_24_a;
sig_24a_kl = sig_24a_ka + pi/2;
sig_24a_kr = sig_24a_ka - pi/2;
stred24 = RWY24a;
stred24(3) = h_kruhova_op;
kruh24 = aproximaceKruhu(stred24, r_kruh, sig_24a_kl, sig_24a_kr,90);

%okoli 12
sig_12a_ka = sig_12_a;
sig_12a_kl = sig_12a_ka + pi/2;
sig_12a_kr = sig_12a_ka - pi/2;
stred12 = RWY12a;
stred12(3) = h_kruhova_op;
kruh12 = aproximaceKruhu(stred12, r_kruh, sig_12a_kl, sig_12a_kr,90);
uzavreniKruhu12 = eastNorth(rajon(r_kruh, sig_12a_kr, stred12));

%okoli 30
sig_30a_ka = sig_30_a;
sig_30a_kl = sig_30a_ka + pi/2;
sig_30a_kr = sig_30a_ka - pi/2;
stred30 = RWY30a;
stred30(3) = h_kruhova_op;
kruh30 = aproximaceKruhu(stred30, r_kruh, sig_30a_kl, sig_30a_kr,90);


%konstrukce polygonu op0624 a op1230 a ulozeni
polygon_k_0624 = [kruh06;kruh24; uzavreniKruhu06];
polygon_k_1230 = [kruh12;kruh30;uzavreniKruhu12];
filename = '/home/michal/Projects/LetisteProjekt/proVypocet/polygony_kruhova_plocha.csv';
ulozPolygon(filename, [polygon_k_0624, polygon_k_1230]);
filename = '/home/michal/Projects/LetisteProjekt/proVypocet/vrstevnice_kruhova_plocha.csv';
ulozJakoVrstevnice(filename, [polygon_k_0624, polygon_k_1230])

%%------------KUZELOVA PLOCHA---------------------------------------------------
r_kruh_kuzel = 6000;
h_kruh_kuzel = 100;
h_kuzelova_op = h_kruhova_op + h_kruh_kuzel;%vyska konce kuzelove plochy

%okoli 06
stred06_kuzel = stred06;
stred06_kuzel(3) = h_kuzelova_op;
kruh06_kuzel = aproximaceKruhu(stred06_kuzel, r_kruh_kuzel, sig_06a_kl, sig_06a_kr,90);
uzavreniKruhuKuzel06 = eastNorth(rajon(r_kruh_kuzel, sig_06a_kr, stred06_kuzel));

%okoli 24
stred24_kuzel = stred24;
stred24_kuzel(3) = h_kuzelova_op;
kruh24_kuzel = aproximaceKruhu(stred24_kuzel, r_kruh_kuzel, sig_24a_kl, sig_24a_kr,90);

%okoli 12
stred12_kuzel = stred12;
stred12_kuzel(3) = h_kuzelova_op;
kruh12_kuzel = aproximaceKruhu(stred12_kuzel, r_kruh_kuzel, sig_12a_kl, sig_12a_kr,90);
uzavreniKruhuKuzel12 = eastNorth(rajon(r_kruh_kuzel, sig_12a_kr, stred12_kuzel));

%okoli 30
stred30_kuzel = stred30;
stred30_kuzel(3) = h_kuzelova_op;
kruh30_kuzel = aproximaceKruhu(stred30_kuzel, r_kruh_kuzel, sig_30a_kl, sig_30a_kr,90);


%konstrukce polygonu a ulození
polygon_kuzel_0624 = [kruh06_kuzel;kruh24_kuzel;uzavreniKruhuKuzel06];
polygon_kuzel_1230 = [kruh12_kuzel;kruh30_kuzel;uzavreniKruhuKuzel12];
filename = '/home/michal/Projects/LetisteProjekt/modelDrah/polygony_kuzel_OP.csv';
ulozPolygonSDirou(filename, [polygon_kuzel_0624,polygon_kuzel_1230], [polygon_k_0624,polygon_k_1230]);

filename = '/home/michal/Projects/LetisteProjekt/proVypocet/polygon_kuzelova_plocha_0624.csv';
ulozPolygonSDirou(filename, [polygon_kuzel_0624], [polygon_k_0624]);
filename = '/home/michal/Projects/LetisteProjekt/proVypocet/polygon_kuzelova_plocha_1230.csv';
ulozPolygonSDirou(filename, [polygon_kuzel_1230], [polygon_k_1230]);

filename = '/home/michal/Projects/LetisteProjekt/proVypocet/vrstevnice_kuzelova_plocha_0624.csv';
ulozJakoVrstevnice(filename, [polygon_kuzel_0624, polygon_k_0624]);
filename = '/home/michal/Projects/LetisteProjekt/proVypocet/vrstevnice_kuzelova_plocha_1230.csv';
ulozJakoVrstevnice(filename, [polygon_kuzel_1230, polygon_k_1230]);

%%%------------PRECHODOVA PLOCHA-------------------------------------------------
%%%------------OLD DEPRECATED-------------------
%h_prechod_06 = h_kruhova_op - RWY06(3);
%h_prechod_12 = h_kruhova_op - RWY12(3);
%h_prechod_24 = h_kruhova_op - RWY24(3);
%h_prechod_30 = h_kruhova_op - RWY30(3);
%k_stoupani_prechod = 7;
%d_prechod_06 = h_prechod_06 * k_stoupani_prechod;
%d_prechod_12 = h_prechod_12 * k_stoupani_prechod;
%d_prechod_24 = h_prechod_24 * k_stoupani_prechod;
%d_prechod_30 = h_prechod_30 * k_stoupani_prechod;
%%%------------OLD DEPRECATED-------------------
%%polygony a linie
%RWY06al_prechod = sikmyRajon(d_prechod_06, sig_06al_el, RWY06al,h_prechod_06); %tohle je špatně
%RWY24ar_prechod = sikmyRajon(d_prechod_24, sig_24ar_er, RWY24ar,h_prechod_24); %tohle taky
%polygon_prechod_06l_24r = [eastNorth(RWY06al);eastNorth(RWY06al_prechod);eastNorth(RWY24ar_prechod);eastNorth(RWY24ar);eastNorth(RWY06al)];
%h_0624 = RWY06(3) - RWY24(3);
%d_prechod_0624 = h_0624 * k_stoupani_prechod;
%RWY24ar_h06 = sikmyRajon(d_prechod_0624, sig_24ar_er, RWY24ar,h_0624);
%line_prechod_0624 = [eastNorth(RWY06al_prechod);eastNorth(RWY24ar_prechod)];
%line_prechod_h0624 = [eastNorth(RWY06l);eastNorth(RWY24ar_h06)];
%line_prechod_24r = [eastNorth(RWY24r);eastNorth(RWY24ar)];
%%%------------OLD DEPRECATED-------------------
%RWY06ar_prechod = sikmyRajon(d_prechod_06, sig_06ar_er, RWY06ar, h_prechod_06);
%RWY24al_prechod = sikmyRajon(d_prechod_24, sig_24al_el, RWY24al, h_prechod_24);
%polygon_prechod_24l_06r = [eastNorth(RWY24al);eastNorth(RWY24al_prechod);eastNorth(RWY06ar_prechod);eastNorth(RWY06ar);eastNorth(RWY24al)];
%RWY24al_h06 = sikmyRajon(d_prechod_0624, sig_24al_el, RWY24al,h_0624);
%line_prechod_2406 = [eastNorth(RWY06ar_prechod);eastNorth(RWY24al_prechod)];
%line_prechod_h2406 = [eastNorth(RWY06r);eastNorth(RWY24al_h06)];
%line_prechod_24l = [eastNorth(RWY24l);eastNorth(RWY24al)];
%%%------------OLD DEPRECATED-------------------
%RWY12al_prechod = sikmyRajon(d_prechod_12, sig_12al_el, RWY12al,h_prechod_12);
%RWY30ar_prechod = sikmyRajon(d_prechod_30, sig_30ar_er, RWY30ar,h_prechod_30);
%polygon_prechod_12l_30r = [eastNorth(RWY12al);eastNorth(RWY12al_prechod);eastNorth(RWY30ar_prechod);eastNorth(RWY30ar);eastNorth(RWY12al)];
%h_1230 = RWY30(3) - RWY12(3);
%d_prechod_1230 = h_1230 * k_stoupani_prechod;
%RWY12ar_h30 = sikmyRajon(d_prechod_1230, sig_12ar_er,RWY12ar,h_1230);
%line_prechod_1230 = [eastNorth(RWY30ar_prechod);eastNorth(RWY12al_prechod)];
%line_prechod_h1230 = [eastNorth(RWY30l);eastNorth(RWY12ar_h30)];
%line_prechod_12r = [eastNorth(RWY12r);eastNorth(RWY12ar)];
%%%------------OLD DEPRECATED-------------------
%RWY30al_prechod = sikmyRajon(d_prechod_30, sig_30al_el, RWY30al,h_prechod_30);
%RWY12ar_prechod = sikmyRajon(d_prechod_12, sig_12ar_er, RWY12ar,h_prechod_12);
%polygon_prechod_30l_12r = [eastNorth(RWY30al);eastNorth(RWY30al_prechod);eastNorth(RWY12ar_prechod);eastNorth(RWY12ar);eastNorth(RWY30al)];
%RWY12al_h30 = sikmyRajon(d_prechod_1230, sig_12al_el,RWY12al,h_1230);
%line_prechod_3012 = [eastNorth(RWY30al_prechod);eastNorth(RWY12ar_prechod)];
%line_prechod_h3012 = [eastNorth(RWY30r);eastNorth(RWY12al_h30)];
%line_prechod_12l = [eastNorth(RWY12l);eastNorth(RWY12al)];
%%%------------OLD DEPRECATED-------------------
%%ulozeni do souboru
%filename = '/home/michal/Projects/LetisteProjekt/modelDrah/polygony_prechod_OP.csv';
%ulozPolygon(filename,[polygon_prechod_06l_24r,polygon_prechod_12l_30r,polygon_prechod_24l_06r, polygon_prechod_30l_12r]);
%%%------------OLD DEPRECATED-------------------
%filename = '/home/michal/Projects/LetisteProjekt/proVypocet/vrstevnice_prechodova_plocha_06l24r.csv';
%ulozJakoVrstevnice(filename, [line_prechod_0624,line_prechod_h0624,line_prechod_24r]);
%filename = '/home/michal/Projects/LetisteProjekt/proVypocet/polygon_prechodova_plocha_06l24r.csv';
%ulozPolygon(filename, polygon_prechod_06l_24r);
%%%------------OLD DEPRECATED-------------------
%filename = '/home/michal/Projects/LetisteProjekt/proVypocet/vrstevnice_prechodova_plocha_06r24l.csv';
%ulozJakoVrstevnice(filename, [line_prechod_2406,line_prechod_h2406,line_prechod_24l]);
%filename = '/home/michal/Projects/LetisteProjekt/proVypocet/polygon_prechodova_plocha_06r24l.csv';
%ulozPolygon(filename, polygon_prechod_24l_06r);
%%%------------OLD DEPRECATED-------------------
%filename = '/home/michal/Projects/LetisteProjekt/proVypocet/vrstevnice_prechodova_plocha_12r30l.csv';
%ulozJakoVrstevnice(filename, [line_prechod_1230,line_prechod_h1230,line_prechod_12r]);
%filename = '/home/michal/Projects/LetisteProjekt/proVypocet/polygon_prechodova_plocha_12r30l.csv';
%ulozPolygon(filename, polygon_prechod_30l_12r);
%%%------------OLD DEPRECATED-------------------
%filename = '/home/michal/Projects/LetisteProjekt/proVypocet/vrstevnice_prechodova_plocha_12l30r.csv';
%ulozJakoVrstevnice(filename, [line_prechod_3012,line_prechod_h3012,line_prechod_12l]);
%filename = '/home/michal/Projects/LetisteProjekt/proVypocet/polygon_prechodova_plocha_12l30r.csv';
%ulozPolygon(filename, polygon_prechod_12l_30r);
%%%------------OLD DEPRECATED-------------------


%%------------VNEJSI VODOROVNA PLOCHA-------------------------------------------
r_kruh_out = 9000;
h_op_op = h_kuzelova_op;

%okoli 06
kruh06_out = aproximaceKruhu(stred06_kuzel, r_kruh_out, sig_06a_kl, sig_06a_kr,90);
uzavreniKruhuOut06 = eastNorth(rajon(r_kruh_out, sig_06a_kr, stred06_kuzel));

%okoli 24
kruh24_out = aproximaceKruhu(stred24_kuzel, r_kruh_out, sig_24a_kl, sig_24a_kr,90);

%okoli 12
kruh12_out = aproximaceKruhu(stred12_kuzel, r_kruh_out, sig_12a_kl, sig_12a_kr,90);
uzavreniKruhuOut12 = eastNorth(rajon(r_kruh_out, sig_12a_kr, stred12_kuzel));

%okoli 30
kruh30_out = aproximaceKruhu(stred30_kuzel, r_kruh_out, sig_30a_kl, sig_30a_kr,90);


%konstrukce polygonu a ulozeníRWY06t = sikmyRajon(d_top, sig_06a_e, RWY06a, h_top);

polygon_out_0624 = [kruh06_out;kruh24_out;uzavreniKruhuOut06];
polygon_out_1230 = [kruh12_out;kruh30_out;uzavreniKruhuOut12];
filename = '/home/michal/Projects/LetisteProjekt/proVypocet/polygony_vnejsich_ploch.csv';
ulozPolygonSDirou(filename, [polygon_out_0624, polygon_out_1230], [polygon_kuzel_0624,polygon_kuzel_1230]);
filename = '/home/michal/Projects/LetisteProjekt/proVypocet/vrstevnice_vnejsi_plocha_0624.csv';
ulozJakoVrstevnice(filename, [polygon_out_0624,polygon_kuzel_0624]);
filename = '/home/michal/Projects/LetisteProjekt/proVypocet/vrstevnice_vnejsi_plocha_1230.csv';
ulozJakoVrstevnice(filename, [polygon_out_1230,polygon_kuzel_1230]);
filename = '/home/michal/Projects/LetisteProjekt/proVypocet/vrstevnice_vnejsich_ploch.csv';
ulozJakoVrstevnice(filename, [polygon_out_0624, polygon_out_1230, polygon_kuzel_0624,polygon_kuzel_1230]);

%%------------NOVA PRECHODOVA PLOCHA--------------------------------------------
k_stoupani_prechod = 7;
dh_06 = h_kuzelova_op - RWY06(3);
d_06_06h = k_stoupani_prechod * dh_06;
dh_24 = h_kuzelova_op - RWY24(3);
d_24_24h = k_stoupani_prechod * dh_24;
dh_12 = h_kuzelova_op - RWY12(3);
d_12_12h = k_stoupani_prechod * dh_12;
dh_30 = h_kuzelova_op - RWY30(3);
d_30_30h = k_stoupani_prechod * dh_30;
dh_06_24 = RWY06(3) - RWY24(3);
d_24_06h = k_stoupani_prechod * dh_06_24;
dh_30_12 = RWY30(3) - RWY12(3);
d_12_30h = k_stoupani_prechod * dh_30_12;

RWY06thl = rajon(d_top, sig_06_a, RWY06al);
RWY06thr = rajon(d_top, sig_06_a, RWY06ar);
RWY24thl = rajon(d_top, sig_24_a, RWY24al);
RWY24thr = rajon(d_top, sig_24_a, RWY24ar);
RWY12thl = rajon(d_top, sig_12_a, RWY12al);
RWY12thr = rajon(d_top, sig_12_a, RWY12ar);
RWY30thl = rajon(d_top, sig_30_a, RWY30al);
RWY30thr = rajon(d_top, sig_30_a, RWY30ar);

RWY06lhkp = sikmyRajon(d_06_06h, sig_06_l, RWY06l, dh_06);
RWY06thlhkp = sikmyRajon(d_06_06h, sig_06_l, RWY06thl, dh_06);
RWY06rhkp = sikmyRajon(d_06_06h, sig_06_r, RWY06r, dh_06);
RWY06thrhkp = sikmyRajon(d_06_06h, sig_06_r, RWY06thr, dh_06);
RWY12lhkp = sikmyRajon(d_12_12h, sig_12_l, RWY12l, dh_12);
RWY12thlhkp = sikmyRajon(d_12_12h, sig_12_l, RWY12thl, dh_12);
RWY12rhkp = sikmyRajon(d_12_12h, sig_12_r, RWY12r, dh_12);
RWY12thrhkp = sikmyRajon(d_12_12h, sig_12_r, RWY12thr, dh_12);
RWY24lhkp = sikmyRajon(d_24_24h, sig_24_l, RWY24l, dh_24);
RWY24thlhkp = sikmyRajon(d_24_24h, sig_24_l, RWY24thl, dh_24);
RWY24rhkp = sikmyRajon(d_24_24h, sig_24_r, RWY24r, dh_24);
RWY24thrhkp = sikmyRajon(d_24_24h, sig_24_r, RWY24thr, dh_24);
RWY30lhkp = sikmyRajon(d_30_30h, sig_30_l, RWY30l, dh_30);
RWY30thlhkp = sikmyRajon(d_30_30h, sig_30_l, RWY30thl, dh_30);
RWY30rhkp = sikmyRajon(d_30_30h, sig_30_r, RWY30r, dh_30);
RWY30thrhkp = sikmyRajon(d_30_30h, sig_30_r, RWY30thr, dh_30);

%06l 24r 
RWY24rh06l = sikmyRajon(d_24_06h, sig_24_r, RWY24r,dh_06_24);
RWY24r1mhkp = sikmyRajon(7, sig_24_r, RWY24r, 1);
d_06_24 = vzdalenost(RWY06, RWY24);
RWY24r1m06 = sikmyRajon(d_06_24/dh_06_24, sig_06_a, RWY24r, 1);

%vrstevnice pro 3 polygony 06l 24r
vrstevnice_06thl06l = [eastNorth(RWY06thl);eastNorth(RWY06l)];
vrstevnice_06thlhkp06lhkp = [eastNorth(RWY06thlhkp);eastNorth(RWY06lhkp)];
vrstevnice_24thr24r = [eastNorth(RWY24thr);eastNorth(RWY24r)];
vrstevnice_24thrhkp24rhkp = [eastNorth(RWY24thrhkp);eastNorth(RWY24rhkp)];
vrstevnice_06lhkp24rhkp = [eastNorth(RWY06lhkp);eastNorth(RWY24rhkp)];
vrstevnice_06l24rh = [eastNorth(RWY06l);eastNorth(RWY24rh06l)];
vrstevnice_24r1m = [eastNorth(RWY24r1mhkp);eastNorth(RWY24r1m06)];

%polygony 06l 24r
polygon_06l_06thlhkp = [eastNorth(RWY06l);eastNorth(RWY06thl);eastNorth(RWY06thlhkp);eastNorth(RWY06lhkp);eastNorth(RWY06l)];
polygon_24r_24thrhkp = [eastNorth(RWY24r);eastNorth(RWY24thr);eastNorth(RWY24thrhkp);eastNorth(RWY24rhkp);eastNorth(RWY24r)];
polygon_06l_24r = [eastNorth(RWY24r);eastNorth(RWY06l);eastNorth(RWY06lhkp);eastNorth(RWY24rhkp);eastNorth(RWY24r)];

filename = '/home/michal/Projects/LetisteProjekt/proVypocet/vrstevnice_prechodova_plocha_06l06thl.csv';
ulozJakoVrstevnice(filename, [vrstevnice_06thl06l,vrstevnice_06thlhkp06lhkp]);
filename = '/home/michal/Projects/LetisteProjekt/proVypocet/vrstevnice_prechodova_plocha_24r24thr.csv';
ulozJakoVrstevnice(filename, [vrstevnice_24thr24r,vrstevnice_24thrhkp24rhkp]);
filename = '/home/michal/Projects/LetisteProjekt/proVypocet/vrstevnice_prechodova_plocha_06l24r.csv';
ulozJakoVrstevnice(filename, [vrstevnice_06lhkp24rhkp,vrstevnice_06l24rh,vrstevnice_24r1m]);

filename = '/home/michal/Projects/LetisteProjekt/proVypocet/polygon_prechodova_plocha_06l06thl.csv';
ulozPolygon(filename, polygon_06l_06thlhkp);
filename = '/home/michal/Projects/LetisteProjekt/proVypocet/polygon_prechodova_plocha_24r24thr.csv';
ulozPolygon(filename, polygon_24r_24thrhkp);
filename = '/home/michal/Projects/LetisteProjekt/proVypocet/polygon_prechodova_plocha_06l24r.csv';
ulozPolygon(filename, polygon_06l_24r);

%06r 24l
RWY24lh06r = sikmyRajon(d_24_06h, sig_24_l, RWY24l,dh_06_24);
RWY24l1mhkp = sikmyRajon(7, sig_24_l, RWY24l, 1);
RWY24l1m06 = sikmyRajon(d_06_24/dh_06_24, sig_06_a, RWY24l, 1);

%vrstevnice pro 3 polygony 06r 24l
vrstevnice_06thr06r = [eastNorth(RWY06thr);eastNorth(RWY06r)];
vrstevnice_06thrhkp06rhkp = [eastNorth(RWY06thrhkp);eastNorth(RWY06rhkp)];
vrstevnice_24thl24l = [eastNorth(RWY24thl);eastNorth(RWY24l)];
vrstevnice_24thlhkp24lhkp = [eastNorth(RWY24thlhkp);eastNorth(RWY24lhkp)];
vrstevnice_06rhkp24lhkp = [eastNorth(RWY06rhkp);eastNorth(RWY24lhkp)];
vrstevnice_06r24lh = [eastNorth(RWY06r);eastNorth(RWY24lh06r)];
vrstevnice_24l1m = [eastNorth(RWY24l1mhkp);eastNorth(RWY24l1m06)];

%polygony 06r 24l
polygon_06r_06thrhkp = [eastNorth(RWY06r);eastNorth(RWY06thr);eastNorth(RWY06thrhkp);eastNorth(RWY06rhkp);eastNorth(RWY06r)];
polygon_24l_24thlhkp = [eastNorth(RWY24l);eastNorth(RWY24thl);eastNorth(RWY24thlhkp);eastNorth(RWY24lhkp);eastNorth(RWY24l)];
polygon_06r_24l = [eastNorth(RWY24l);eastNorth(RWY06r);eastNorth(RWY06rhkp);eastNorth(RWY24lhkp);eastNorth(RWY24l)];

filename = '/home/michal/Projects/LetisteProjekt/proVypocet/vrstevnice_prechodova_plocha_06r06thr.csv';
ulozJakoVrstevnice(filename, [vrstevnice_06thr06r,vrstevnice_06thrhkp06rhkp]);
filename = '/home/michal/Projects/LetisteProjekt/proVypocet/vrstevnice_prechodova_plocha_24l24thl.csv';
ulozJakoVrstevnice(filename, [vrstevnice_24thl24l,vrstevnice_24thlhkp24lhkp]);
filename = '/home/michal/Projects/LetisteProjekt/proVypocet/vrstevnice_prechodova_plocha_06r24l.csv';
ulozJakoVrstevnice(filename, [vrstevnice_06rhkp24lhkp,vrstevnice_06r24lh,vrstevnice_24l1m]);

filename = '/home/michal/Projects/LetisteProjekt/proVypocet/polygon_prechodova_plocha_06r06thr.csv';
ulozPolygon(filename, polygon_06r_06thrhkp);
filename = '/home/michal/Projects/LetisteProjekt/proVypocet/polygon_prechodova_plocha_24l24thl.csv';
ulozPolygon(filename, polygon_24l_24thlhkp);
filename = '/home/michal/Projects/LetisteProjekt/proVypocet/polygon_prechodova_plocha_06r24l.csv';
ulozPolygon(filename, polygon_06r_24l);

%30l 12r 
RWY12rh30l = sikmyRajon(d_12_30h, sig_12_r, RWY12r,dh_30_12);
RWY12r1mhkp = sikmyRajon(7, sig_12_r, RWY12r, 1);
d_30_12 = vzdalenost(RWY30, RWY12);
RWY12r1m30 = sikmyRajon(d_30_12/dh_30_12, sig_30_a, RWY12r, 1);

%vrstevnice pro 3 polygony 30l 12r
vrstevnice_30thl30l = [eastNorth(RWY30thl);eastNorth(RWY30l)];
vrstevnice_30thlhkp30lhkp = [eastNorth(RWY30thlhkp);eastNorth(RWY30lhkp)];
vrstevnice_12thr12r = [eastNorth(RWY12thr);eastNorth(RWY12r)];
vrstevnice_12thrhkp12rhkp = [eastNorth(RWY12thrhkp);eastNorth(RWY12rhkp)];
vrstevnice_30lhkp12rhkp = [eastNorth(RWY30lhkp);eastNorth(RWY12rhkp)];
vrstevnice_30l12rh = [eastNorth(RWY30l);eastNorth(RWY12rh30l)];
vrstevnice_12r1m = [eastNorth(RWY12r1mhkp);eastNorth(RWY12r1m30)];

%polygony 30l 12r
polygon_30l_30thlhkp = [eastNorth(RWY30l);eastNorth(RWY30thl);eastNorth(RWY30thlhkp);eastNorth(RWY30lhkp);eastNorth(RWY30l)];
polygon_12r_12thrhkp = [eastNorth(RWY12r);eastNorth(RWY12thr);eastNorth(RWY12thrhkp);eastNorth(RWY12rhkp);eastNorth(RWY12r)];
polygon_30l_12r = [eastNorth(RWY12r);eastNorth(RWY30l);eastNorth(RWY30lhkp);eastNorth(RWY12rhkp);eastNorth(RWY12r)];

filename = '/home/michal/Projects/LetisteProjekt/proVypocet/vrstevnice_prechodova_plocha_30l30thl.csv';
ulozJakoVrstevnice(filename, [vrstevnice_30thl30l,vrstevnice_30thlhkp30lhkp]);
filename = '/home/michal/Projects/LetisteProjekt/proVypocet/vrstevnice_prechodova_plocha_12r12thr.csv';
ulozJakoVrstevnice(filename, [vrstevnice_12thr12r,vrstevnice_12thrhkp12rhkp]);
filename = '/home/michal/Projects/LetisteProjekt/proVypocet/vrstevnice_prechodova_plocha_30l12r.csv';
ulozJakoVrstevnice(filename, [vrstevnice_30lhkp12rhkp,vrstevnice_30l12rh,vrstevnice_12r1m]);

filename = '/home/michal/Projects/LetisteProjekt/proVypocet/polygon_prechodova_plocha_30l30thl.csv';
ulozPolygon(filename, polygon_30l_30thlhkp);
filename = '/home/michal/Projects/LetisteProjekt/proVypocet/polygon_prechodova_plocha_12r12thr.csv';
ulozPolygon(filename, polygon_12r_12thrhkp);
filename = '/home/michal/Projects/LetisteProjekt/proVypocet/polygon_prechodova_plocha_30l12r.csv';
ulozPolygon(filename, polygon_30l_12r);

%30r 12l
RWY12lh30r = sikmyRajon(d_12_30h, sig_12_l, RWY12l,dh_30_12);
RWY12l1mhkp = sikmyRajon(7, sig_12_l, RWY12l, 1);
RWY12l1m30 = sikmyRajon(d_30_12/dh_30_12, sig_30_a, RWY12l, 1);

%vrstevnice pro 3 polygony 30r 12l
vrstevnice_30thr30r = [eastNorth(RWY30thr);eastNorth(RWY30r)];
vrstevnice_30thrhkp30rhkp = [eastNorth(RWY30thrhkp);eastNorth(RWY30rhkp)];
vrstevnice_12thl12l = [eastNorth(RWY12thl);eastNorth(RWY12l)];
vrstevnice_12thlhkp12lhkp = [eastNorth(RWY12thlhkp);eastNorth(RWY12lhkp)];
vrstevnice_30rhkp12lhkp = [eastNorth(RWY30rhkp);eastNorth(RWY12lhkp)];
vrstevnice_30r12lh = [eastNorth(RWY30r);eastNorth(RWY12lh30r)];
vrstevnice_12l1m = [eastNorth(RWY12l1mhkp);eastNorth(RWY12l1m30)];

%polygony 30r 12l
polygon_30r_30thrhkp = [eastNorth(RWY30r);eastNorth(RWY30thr);eastNorth(RWY30thrhkp);eastNorth(RWY30rhkp);eastNorth(RWY30r)];
polygon_12l_12thlhkp = [eastNorth(RWY12l);eastNorth(RWY12thl);eastNorth(RWY12thlhkp);eastNorth(RWY12lhkp);eastNorth(RWY12l)];
polygon_30r_12l = [eastNorth(RWY12l);eastNorth(RWY30r);eastNorth(RWY30rhkp);eastNorth(RWY12lhkp);eastNorth(RWY12l)];

filename = '/home/michal/Projects/LetisteProjekt/proVypocet/vrstevnice_prechodova_plocha_30r30thr.csv';
ulozJakoVrstevnice(filename, [vrstevnice_30thr30r,vrstevnice_30thrhkp30rhkp]);
filename = '/home/michal/Projects/LetisteProjekt/proVypocet/vrstevnice_prechodova_plocha_12l12thl.csv';
ulozJakoVrstevnice(filename, [vrstevnice_12thl12l,vrstevnice_12thlhkp12lhkp]);
filename = '/home/michal/Projects/LetisteProjekt/proVypocet/vrstevnice_prechodova_plocha_30r12l.csv';
ulozJakoVrstevnice(filename, [vrstevnice_30rhkp12lhkp,vrstevnice_30r12lh,vrstevnice_12l1m]);

filename = '/home/michal/Projects/LetisteProjekt/proVypocet/polygon_prechodova_plocha_30r30thr.csv';
ulozPolygon(filename, polygon_30r_30thrhkp);
filename = '/home/michal/Projects/LetisteProjekt/proVypocet/polygon_prechodova_plocha_12l12thl.csv';
ulozPolygon(filename, polygon_12l_12thlhkp);
filename = '/home/michal/Projects/LetisteProjekt/proVypocet/polygon_prechodova_plocha_30r12l.csv';
ulozPolygon(filename, polygon_30r_12l);